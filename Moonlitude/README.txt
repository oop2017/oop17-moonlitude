File con la descrizione del progetto: autori, breve guida d�uso, link a risorse.

res Risorse
lib Librerie esterne (es: file .jar .so .dll)
bin Binario (Con copia delle risorse)
doc Documentazione
	Se si decide di inserire sia documentazione di tipo non rigenerabile 
	(e.g. una relazione in pdf) sia quella di tipo rigenerabile (e.g. la javadoc), 
	`e bene tenerle separate. La documentazione rigenerabile non va tracciata col DVCS